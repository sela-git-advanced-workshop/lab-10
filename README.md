# Advanced Git Workshop
Lab 10: Removing files and sensitive data

---

# Tasks

 - Adding sensitive data
 
 - Removing sensitive data
 
 - Removing files

---

## Preparations

 - Let's clone a repository to work with:
 
```
$ git clone https://oauth2:gkLjhh5W4te-cJngRBxB@gitlab.com/sela-git-advanced-workshop/static-web-app.git lab10
$ cd lab10
```

---

## Adding sensitive data

 - Let's add **<meta name="password" content="SENSITIVE_DATA">** to the **index.html** file:
```
<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="password" content="SENSITIVE_DATA">
    <meta name="author" content="">
```

 - Add and commit the changes:
```
$ git add -A
$ git commit -m "add sensitive data"
```

 - Then perform a regular update, fill the author property in the **index.html** file:
```
<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="password" content="SENSITIVE_DATA">
    <meta name="author" content="AUTHOR">
```

  - Add and commit the changes:
```
$ git add -A
$ git commit -m "regular change"
```

  - The sensitive data shouldn't be there so let's replace it with the word **SECRET**:
```
<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="password" content="SECRET">
    <meta name="author" content="AUTHOR">
```

  - Add and commit the changes:
```
$ git add -A
$ git commit -m "replace sensitive data"
```

---

## Removing sensitive data

 - The sensitive data was removed from the last commit but it still exist in the repository history
  
 - Before removing the sensitive data from the history as well, let's inspect the history:
```
$ git log -n 30 --oneline
```
```
9a89db4 (HEAD -> master) replace sensitive data
1637cb7 regular change
a6f9046 add sensitive data
bf19d7e (origin/master, origin/HEAD) update page title
770ab72 update nav title
d08de8e style improvements
d5a9a01 update bug
dc49be7 update footer
958d97c update main icon
92fc818 update contact section
30b017d update info section
b8a66cd update pictures section
5f372c5 introduce bug
fd9b95a update main title description
04ea586 update main title
8d01948 update nav item
5eeb955 update nav item
17cc553 update nav title
5ac1c83 update page title
960fede init
```

 - To replace the sensitive data rewriting the history use:
```
$ git filter-branch --tree-filter "find index.html -exec sed -i -e 's/SENSITIVE_DATA/SECTRET/g' {} \;"
```

 - Inspect the history and note that the commits that contained the sensitive data were rewritten:
```
$ git log -n 30 --oneline
```

```
0df6554 (HEAD -> master) replace sensitive data
4d93847 regular change
e3e89e5 add sensitive data
bf19d7e (origin/master, origin/HEAD) update page title
770ab72 update nav title
d08de8e style improvements
d5a9a01 update bug
dc49be7 update footer
958d97c update main icon
92fc818 update contact section
30b017d update info section
b8a66cd update pictures section
5f372c5 introduce bug
fd9b95a update main title description
04ea586 update main title
8d01948 update nav item
5eeb955 update nav item
17cc553 update nav title
5ac1c83 update page title
960fede init
```

 - Checkout the "add sensitive data" and ensure the password was replaced:
```
$ git checkout HEAD~2
$ cat index.html  | grep password
```

---

## Removing files

  - Now we will remove all the **mail** folder from the entire history
```
$ git filter-branch -f --index-filter 'git rm -r --ignore-unmatch mail' HEAD
```
